const express = require('express')
const app = express()
const imagemin = require('imagemin')
const imageminMozjpeg = require('imagemin-mozjpeg')
const pngToJpeg = require('png-to-jpeg')
const Jimp = require('jimp')

app.get('/', (req, res) => {
    imagemin(['img/*.{jpg,png}'], {
        destination: 'build',
        plugins: [
            imageminMozjpeg({
                quality: 50
            })]
    })
        .then((files) => {
            console.log({ data: files });
            res.json({ msg: 'compressed' })
        });
});

app.get('/api', (req, res) => {
    Jimp.read("img/1.png", function (err, files) {
        if (err) throw err;
        files
            .quality(60)
            .write("build/2.jpg", function (err) {
                if (err) console.log(err)
                console.log('done')
                res.send('done')
            }); 
    });
});

app.listen(3000, () => {
    console.log('server running on port 3000')
});